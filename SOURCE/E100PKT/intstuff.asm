; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様様様様様融
;�       GET_E100_INT       �
;藩様様様様様様様様様様様様夕
get_e100_int:
    ; Returns AL = int_no, or ZF=0 if not found.
    ; Note: See the assumptions in get_args.asm
    mov     ax, 0x3560
    mov     bp, valid_sw_vecs + 1
    push    es
.loop:
    mov     cx, tsr.sig_end - tsr.sig
    int     0x21            ; ES:BX = handler
    mov     si, tsr.sig
    lea     di, [bx + 3]

    cmp     di, si
    jne     .loopbottom
    repe    cmpsb
    je      .done
.loopbottom:
    mov     si, bp
    lodsb
    inc     bp
    cmp     al, 0x60
    jnc     .loop
.done:
    pop     es
    ret

;浜様様様様様様様様様様様様様様�
;�       GET_IRQ_OR_QUIT       �
;藩様様様様様様様様様様様様様様�
get_irq_or_quit:
    mov     bl, 0x3c    ; Interrupt Line Register
    call    get_pci_var
    mov     dx, .irq_is
    call    print_str_byte

    cmp     al, 0xf
    mov     dx, .bad_irq_str
    jnbe    print_message_and_quit

    add     al, 8
    cmp     al, 16
    jb      .handle_mask
    mov     byte [pic_port_lo], 0xa0
    add     al, 0x60
    mov     word [out_or_nop], 0x20e6   ; OUT 0x20,AL

.handle_mask:
    mov     dx, .aka_int
    call    print_str_byte
    mov     byte [irq_int], al

    ; irq mask = ~(1 << (irq # & 7))
    and     al, 7
    mov     cl, 1
    xchg    ax, cx
    shl     al, cl
    xor     byte [irq_mask], al ; FF gets exactly one bit flipped
    jmp     crlf

.irq_is:            db "The BIOS configured the adapter to use IRQ $"
.bad_irq_str:       db `.\r\n\r\nError! Invalid IRQ.$`
.aka_int:           db ", which is INT $"

;浜様様様様様様様様様様様融
;�       UNMASK_IRQ       �
;藩様様様様様様様様様様様夕
irq_mask                   equ unmask_irq.x + 1
unmask_irq:
    mov     dh, 0
    mov     dl, byte [pic_port_lo]
    inc     dx
    in      al, dx
.x: and     al, 0xff    ; get_irq_or_quit() makes one of the bits a 0
    out     dx, al
    ret

