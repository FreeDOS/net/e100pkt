@echo off
if not "%2"=="" goto usage
if "%1"=="package" goto package
if not "%1"=="" goto usage

call nasm -f bin -l e100pkt.lst -o e100pkt.com _head.asm
goto end

:package
REM
REM Update version in Function #1 (info)
REM Update version and copyright in tail.asm
REM Update version and copyright in manpage
REM Update version and date in LSM
REM
if not exist ..\..\appinfo\e100pkt.lsm set errmsg=Error: Need ..\..\appinfo\e100pkt.lsm
if not exist ..\..\doc\e100pkt.doc set errmsg=Error: Need ..\..\doc\e100pkt.doc
if not exist ..\..\drivers\e100pkt\nul set errmsg=Error: Need ..\..\drivers\e100pkt directory
if not exist ..\..\source\e100pkt\nul set errmsg=Error: Need ..\..\source\e100pkt directory
if not exist eeprom.asm set errmsg=Error: Current directory should contain the ASM files
if not exist make.bat set errmsg=Error: Current directory should contain this batch file
if not "%errmsg%"=="" goto err

call nasm -f bin -o e100pkt.com _head.asm
if errorlevel 1 goto end
if exist ..\..\drivers\e100pkt\e100pkt.com del ..\..\drivers\e100pkt\e100pkt.com > nul
if exist e100pkt.lst del e100pkt.lst > nul
if exist e100.pci del e100.pci > nul
if exist e100.csr del e100.csr > nul
if exist e100.eep del e100.eep > nul
move e100pkt.com ..\..\drivers\e100pkt

cd ..\..
if exist source\e100pkt.zip del source\e100pkt.zip > nul
zip -9 -r source\e100pkt.zip appinfo\e100pkt.lsm doc\e100pkt.doc drivers\e100pkt source\e100pkt
cd source
goto end

:usage
echo Usage: make [package]
echo With the package argument, it builds a FreeDOS package
echo Without it, it simply assembles the driver
goto end
:err
echo %errmsg%
set errmsg=
:end
