; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

cpu     8086

;浜様様様様様様様様様様様様様様融
;�       CPU_TYPE_OR_QUIT       �
;藩様様様様様様様様様様様様様様夕
cpu_type_or_quit:
    mov     ah, 0x30
    int     0x21
    cmp     al, 0
    mov     dx, .need_dos2_str
    jne     .got_dos2
    call    print_str
    int     0x20        ; INT 21/4C requires DOS 2.

.got_dos2:
    ; When pushing SP, the 8088 decrements it beforehand;
    ; everything else decrements it after.
    mov     dx, .need386str
    push    sp
    pop     ax
    cmp     ax, sp
    jne     print_message_and_quit

    ; Everything above the 8088 invokes interrupt 6 on an invalid
    ; instruction.
    push    dx              ; Stash .need386str
    mov     ax, 0x3506
    int     0x21
    push    es, bx          ; Stash old INT 6 vector
    mov     ah, 0x25
    mov     dx, .int6_handler
    int     0x21            ; And set the new one
cpu 386
    ; Testing: LEA AX,AX
    ;db 0x8d,0xc0
    cwde                    ; Invoke INT 6 if neither 386+ nor 8088
cpu 8086
.testdone:
    pop     dx, ds
    int     0x21            ; Restore old INT 6 vector
    push    cs, cs
    pop     ds, es, dx      ; Restore segs, DX = .need386str
.retinst:
    ret
    jmp     print_message_and_quit

.int6_handler:
    pop     cx
    mov     cx, .testdone
    push    cx
    mov     byte [.retinst], 0x90
    iret

.need386str: db `\r\nError: An 80386 or compatible processor is `
             db `required.$`
.need_dos2_str: db `\r\nError: DOS 2 or higher is required.$`

;浜様様様様様様様様様様様様様様様様様融
;�       PRINT_MESSAGE_AND_QUIT       �
;藩様様様様様様様様様様様様様様様様様夕
print_message_and_quit:
    ; DS:DX = message
    call    print_str
quit_error:
    call    timer_cleanup
    mov     ax, 0x4c01
    int     0x21

print_message_and_quit_okay:
    ; DS:DX = message
    call    print_str
quit_okay:
    call    timer_cleanup
    mov     ax, 0x4c00
    int     0x21

cpu     386
