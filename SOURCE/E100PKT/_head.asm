; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

    cpu     8086
    org     0x100
mac_address:
    jmp     start
    times 6 - ($ - mac_address) db 0
    cpu     386

;浜様様様様様様様様�
;�       TSR       �
;藩様様様様様様様様�
tsr:
    jmp     .sig_end
    nop
.sig:
    db      "PKT DRVR",0
.sig2:
    db      "Intel E100 (8255x Family)",0
.sig_end:
    ; When called, the stack is:
    ; - flags   BP + 8
    ; - cs      BP + 6
    ; - ip      BP + 4
    ; - bp      BP + 2
    ; - bx      BP + 0
    sti
    cld         ; Assumed everywhere
    push    bp
    push    bx
    mov     bp, sp

    and     byte [bp + 8], ~1   ; Assume no error- CLC
    mov     bl, ERROR_BAD_COMMAND
    cmp     ah, 8
    jnc     .err
    movzx   bx, ah
    add     bx, bx
    call    word [cs:bx + .func_table]
    jnc     .done
.err:
    or      byte [bp + 8], 1    ; STC
    mov     dh, bl              ; Error code
.done:
    pop     bx
    pop     bp
    iret

.func_table: dw f_err_bad_command ; AH = 0
             dw f_driver_info     ; AH = 1
             dw f_access_type     ; AH = 2
             dw f_release_type    ; AH = 3
             dw f_send_pkt        ; AH = 4
             dw f_terminate       ; AH = 5
             dw f_get_address     ; AH = 6
             dw f_reset_interface ; AH = 7

%include "_common.asm"  ; These two are next to each other to save
%include "_1info.asm"   ; a couple bytes in a JMP instruction
%include "_fr.asm"
%include "_handle.asm"
%include "_hw_int.asm"
%include "_rfd.asm"

%include "_2access.asm"
%include "_3releas.asm"
%include "_4send.asm"
%include "_5termi.asm"
%include "_6gaddr.asm"
%include "_7reset.asm"

absolute $
    alignb 2
    rfds: resb 16 + 1514
    tsr_end:
section .text

%include "tail.asm"
