; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様融
;�       LOAD       �
;藩様様様様様様様様夕
load:
    call    get_e100_int
    jnz     .ok
    mov     dx, .already
    call    print_str_byte
    jmp     quit_error
.ok:
    mov     al, byte [pkt_int]
    mov     ah, 0x35            ; Get vector
    int     0x21
    mov     ax, es
    or      ax, bx
    push    cs
    pop     es
    mov     dx, .occupied
    jnz     print_message_and_quit

    call    detect_pci_bus_or_quit
    call    detect_e100_or_quit
    call    get_irq_or_quit
    call    get_csr_io_bar_or_quit
    call    timer_init
    call    self_test_or_quit
    call    get_mac_address_or_quit
    call    timer_cleanup

    mov     dx, .doingit0
    mov     al, byte [pkt_int]
    call    print_str_byte
    mov     dx, .doingit1
    mov     ax, cs
    call    print_str_word

    mov     ah, 0x49
    mov     es, word [0x2c]
    int     0x21            ; Free environment strings

    xor     eax, eax
    mov     ax, cs
    shl     eax, 4
    mov     bx, 0x06    ; RUC = Load RU Base
    call    scb_command
    mov     bl, 0x60    ; CUC = Load CU Base (BH already 0),
    call    scb_command ; EAX is same as above

    push    cs
    pop     es
    mov     al, 0
    mov     di, handles
    mov     cx, 16 * 8
    rep     stosb

    mov     al, byte [irq_int]
    mov     ah, 0x35
    int     0x21
    mov     word [prev_hw_int], bx
    mov     word [prev_hw_int + 2], es
    mov     ah, 0x25
    mov     dx, hw_int
    int     0x21
    mov     al, byte [pkt_int]
    mov     dx, tsr
    int     0x21

    call    unmask_irq
    call    reset_rfds
    mov     dx, tsr_end + 1
    int     0x27
.doingit0: db 13,10,"Everything seems okay...Loading E100PKT at INT $"
.doingit1: db " using segment $"
.already:  db 13,10,"Error: E100PKT is already loaded at INT $"
.occupied: db 13,10,"Error: The requested vector is occupied "
           db "(non-null)$"
