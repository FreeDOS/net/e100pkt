; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.


; With INT 15/86, loading the driver took a good 3 seconds.
; With PIT reprogramming, loading the driver takes < 1 second.

;浜様様様様様様様様様様様融
;�       TIMER_INIT       �
;藩様様様様様様様様様様様夕
timer_init:
    ; counter   period
    ; 65536     55.55ms
    ; 4         3.34us
    mov     byte [timer_cleanup], 0x06  ; push es

    mov     ax, 2
    mov     ds, ax              ; 2:0 = 0:20 = INT 8
    xor     si, si
    mov     di, timer_int.old
    mov     bl, 4
    call    timer_setup

    xor     di, di
    mov     word [di], timer_int
    mov     word [di + 2], es
    sti
    push    cs
    pop     ds
    ret

;浜様様様様様様様様様様様様様�
;�       TIMER_CLEANUP       �
;藩様様様様様様様様様様様様様�
timer_cleanup:
    ret         ; timer_init changes this to "push es" (06)
    mov     byte [timer_cleanup], 0xc3  ; ret

    xor     bx, bx
    mov     es, bx
    mov     si, timer_int.old
    mov     di, 32
    call    timer_setup

    sti
    pop     es

    ; On my machine, reprogramming the timer causes bit 1
    ; in the IRQ mask to be set!!!
    in      al, 0x21
    and     al, ~2
    out     0x21, al
    ret

;浜様様様様様様様様様様様様�
;�       TIMER_SETUP       �
;藩様様様様様様様様様様様様�
timer_setup:
    ; Helper for timer_init and timer_cleanup
    ; BL = LSB of counter
    ; DS:SI = src dword, ES:DI = dest dword
    mov     al, 0x34
    cli
    out     0x43, al
    xchg    ax, bx
    out     0x40, al
    mov     al, 0
    out     0x40, al

    movsw
    movsw
    ret ; STI is the caller's responsibility

;浜様様様様様様様様様様様�
;�       TIMER_INT       �
;藩様様様様様様様様様様様�
timer_int:
    inc     word [cs:.counter]
    dec     word [cs:.call_old_counter]
    jz      .call_old

    push    ax
    mov     al, 0x20
    out     0x20, al
    pop     ax
    iret

.call_old:
    mov     byte [cs:.call_old_counter + 1], 0x40 ; (LSB already 0)
    db      0xea    ; JMP IMMED32
.old: dd 0
.call_old_counter: dw 0x4000
.counter: dw 0

;浜様様様様様様様様様様様融
;�       SLEEP_10MS       �
;藩様様様様様様様様様様様夕
sleep_10ms:
    ; 3.34us = 1 tick => 10ms = 2994 ticks
    push    cx
    mov     cx, 2994
    jmp     sleep_3us.gotcx

;浜様様様様様様様様様様様�
;�       SLEEP_3US       �
;藩様様様様様様様様様様様�
sleep_3us:
    push    cx
    mov     cx, 2
.gotcx:
    push    ax, bx
    mov     ax, word [timer_int.counter]
.loop:
    mov     bx, word [timer_int.counter]
    sub     bx, ax
    cmp     bx, cx
    jb      .loop

    pop     bx, ax, cx
    ret
