; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様様�
;�       START       �
;藩様様様様様様様様様�
start:
cpu     8086
    cmp     sp, end_of_bss + 1024 * 4
    mov     dx, .nomem
    jb      print_str

    cld             ; Assumed everywhere
    mov     dx, .copyright_str
    call    print_str
    call    cpu_type_or_quit
cpu     386
    jmp     get_args_or_quit    ; Jumps to unload(), dump(), or load()
.nomem:     db "Error: Not enough memory for the BSS!$"
.copyright_str:
db " 浜様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様融",0xd,0xa
db " �                      E100PKT 0.3, packet driver for DOS                    �",0xd,0xa
db " �              Copyright (C) 2022, Seth Simon (sethsimon@sdf.org).           �",0xd,0xa
db " � Many thanks to Intel for publishing a freely available programming manual  �",0xd,0xa
db " �                                                                            �",0xd,0xa
db " � E100PKT comes with ABSOLUTELY NO WARRANTY. This is free software covered by�",0xd,0xa
db " � GNU GENERAL PUBLIC LICENSE 3, and you are welcome to redistribute it under �",0xd,0xa
db " � certain conditions. See http://www.gnu.org or COPYING file for details.    �",0xd,0xa
db " 藩様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様夕",0xd,0xa
db 0xd,0xa,"$"

;浜様様様様様様様様様融
;�       UNLOAD       �
;藩様様様様様様様様様夕
unload:
    call    get_e100_int                ; AL := int_no
    mov     byte [.int_helper + 1], al
    mov     dx, .not_found
    jnz     print_message_and_quit

    ; ACCESS_TYPE
    mov     ax, 0x0201  ; funct# = 2, if_class=1
    mov     bx, -1      ; if_type = wildcard
    mov     dl, 0       ; if_number = 0
    xor     cx, cx      ; All packets
    mov     di, .receiver_helper
    call    .int_helper
    mov     dx, .no_handle
    jc      print_message_and_quit

    xchg    ax, bx  ; BX = handle
    mov     ah, 5   ; funct# = 5 (terminate)
    call    .int_helper
    mov     dx, .succeed
    jnc     print_message_and_quit_okay

    ; Close the handle if terminate() couldn't close it for us
    mov     ah, 3       ; f# = 3 (release_type), BX is already handle
    call    .int_helper
    mov     dx, .no_termi
    jmp     print_message_and_quit

.not_found: db "Error: Can't find E100PKT$"
.no_handle: db "Error: Failed to allocate a handle$"
.succeed:   db "Successfully unloaded E100PKT$"
.no_termi:  db "Error: terminate() failed$"
.int_helper:
    int     0x60
    ret
.receiver_helper:
    xor     di, di
    mov     es, di
    iret

%include "cputype.asm"
%include "findpci.asm"
%include "get_args.asm"
%include "load.asm"
%include "sleep.asm"
%include "printf.asm"
%include "eeprom.asm"
%include "intstuff.asm"
%include "dump.asm"

section .bss
    end_of_bss:
section .text
