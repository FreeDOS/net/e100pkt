; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様様様様融
;�       RESET_RFDS       �
;藩様様様様様様様様様様様夕
reset_rfds:
    ; MANGLES EAX and BX!
    push    di, es

    mov     di, rfds
    push    cs
    pop     es
    xor     eax, eax
    push    di

    stosw               ; Clear status word = 0
    mov     ah, 0x80
    stosw               ; EL=1 (0x8000)

    add     di, 8       ; Go to +12
    cbw                 ; AX = 0
    stosw               ; Actual Count / F / EOF = 0
    xchg    ax, bx      ; BX = 0
    mov     ax, 1514
    stosw               ; Max count = 1514

    pop     ax          ; EAX = rfds
    inc     bx          ; RUC = RU Start = 1 (xchg'd above)
    call    scb_command

    pop     es, di
    ret
