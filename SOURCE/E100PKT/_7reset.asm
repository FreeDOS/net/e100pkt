; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

f_reset_interface:
    ; BX = handle
    push    eax

    mov     bh, ERROR_CANT_RESET
    call    verify_one_handle
    jc      .done

    mov     bx, 4           ; RUC=RU Abort
    call    scb_command
    call    reset_rfds
    clc
.done:
    pop     eax
    ret

