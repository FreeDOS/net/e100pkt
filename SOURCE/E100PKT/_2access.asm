; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

f_access_type:
    ; AL = interface class
    ; BX = interface type
    ; DL = interface number
    ; DS:SI = packet type
    ; CX = packet type length (0 indicates 'match all')
    ; ES:DI = receiver function
    ;
    ; On error, CF=1 and DH = error code. Else AX = handle
    push    es, di, si

    ; Class must be 1 (DIX) or 11 (802.3)
    mov     bl, 10
    cmp     al, 1
    je      .if_class_okay
    cmp     al, 11
    jne     .err_no_class

.if_class_okay:
    cmp     word [bp], -1       ; Type must be wildcard
    jne     .err_no_type

    test    dl, dl
    jnz     .err_no_number

    cmp     cx, 8
    jnbe    .err_bad_type

    ;
    ; CHECK FOR CONFLICTING HANDLES
    ;
    push    ds
    pop     es
    mov     di, si          ; ES:DI := packet type
    call    find_matching_handle
    jz      .err_type_inuse

    ;
    ; STASH HANDLE INFO
    ;
    call    find_free_handle    ; DI = free handle
    jc      .err_no_space

    push    cs
    pop     es
    inc     di      ; Skip 'valid' for now
    stosb           ; Store 'if_class'
    xchg    ax, cx
    stosb           ; Store 'packet_type_length'
    xchg    ax, cx

    pop     si
    push    si
    times 4 movsw   ; 'packet_type'
    pop     si

    pop     ax
    stosw           ; Receiver offset
    pop     bx
    xchg    ax, bx
    stosw           ; Receiver segment

    and     di, ~15         ; Handle address (also clears CF)
    inc     byte [cs:di]    ; Store 'valid' byte (0->1)
    mov     es, ax
    xchg    ax, di
    mov     di, bx

    inc     byte [cs:nhandles]
    ret

; These assume BL is 10 at entry

.err_no_class:      ; 2 = ERROR_NO_CLASS
    dec     bx
    dec     bx
.err_no_type:       ; 3 = ERROR_NO_TYPE
    dec     bx
    dec     bx
.err_no_number:     ; 4 = ERROR_NO_NUMBER
    dec     bx
.err_bad_type:      ; 5 = ERROR_BAD_TYPE
    shr     bl, 1
    inc     bx
.err_no_space:      ; 9 = ERROR_NO_SPACE
    dec     bx
.err_type_inuse:    ; 10 = ERROR_TYPE_INUSE
    stc
    pop     si
    pop     di
    pop     es
    ret
