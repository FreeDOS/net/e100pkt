; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

f_driver_info:
    ; AL = 255
    ; BX = handle (optional)
    ;
    ; On error, returns CF=1 and DH=error code
    ; Otherwise returns:
    ;   BX = version
    ;   CH = class
    ;   DX = type
    ;   CL = number
    ;   DS:SI = name
    ;   AL = functionality
    cmp     al, 255
    jne     f_err_bad_command
    push    cs
    pop     ds

    cwd                     ; DX = 0 since AH is 1
    dec     dx              ; Type = -1 = wildcard
    mov     ax, 3
    xchg    ax, word [bp]       ; Version 0.3. AX := optional handle

    ; Use the handle's if_class if available, else DIX
    call    verify_handle   ; CF=0 if valid
    cmc
    sbb     ch, ch          ; -1 if valid, else 0
    xchg    ax, si
    inc     si
    and     ch, byte [si]   ; handle's if_class if valid, else 0
    mov     ax, 0x0101      ; Restore AH, AL = functionality = basic
    or      ch, ah          ; no change if valid, else 1. CF := 0

    mov     cl, 0           ; if_number = 0
    mov     si, tsr.sig2    ; DS:SI = name
    ret

