; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

irq_int equ f_terminate.a
pkt_int equ f_terminate.b
f_terminate:
    ; BX = handle
    push    ax
    mov     bh, ERROR_CANT_TERMINATE
    call    verify_one_handle
    jc      .done_popax

    push    ds, es, dx, si, cs
    pop     ds

    ; Make sure no one else hooked the hw vector
    mov     si, cs
    db      0xb8        ; MOV AX,IMMED16
.a: db      0           ; Low byte = irq_int
    db      0x35        ; High byte = Get vector
    int     0x21        ; ES:BX := vector
    xchg    ax, dx      ; Stash irq_int in DX

    mov     ax, es
    sub     ax, si      ; Segment difference
    sub     bx, hw_int  ; Offset difference
    or      ax, bx
    mov     bl, ERROR_CANT_TERMINATE
    stc
    jnz     .done

    ; Disable the hardware
    mov     bl, 4                   ; BH already 0, RUC=RU Abort
    call    scb_command

    mov     dh, 0x25
    xchg    ax, dx              ; AX = 0x25 << 8 + irq_int
    lds     dx, [prev_hw_int]
    int     0x21

    db      0xb0                ; MOV AL,IMMED8
.b: db      0                   ; pkt_int
    cwd                         ; DX = 0
    mov     ds, dx
    int     0x21

    ; ES is already the same as CS
    mov     ah, 0x49
    int     0x21                    ; Free memory (returns CF=0)
.done:
    pop     si, dx, es, ds
.done_popax:
    pop     ax
    ret

