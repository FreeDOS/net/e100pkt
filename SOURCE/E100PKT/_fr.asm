; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様様様様�
;�       HANDLE_FR       �
;藩様様様様様様様様様様様�
handle_fr:
    ; Wait for EOF and F to become 1
    mov     di, rfds + 28
.wait:
    mov     bx, word [di - 16]
    sub     bh, 0xc0
    jc      .wait

    ; Bit 5 of the status byte (at [rfd]) is allegedly 1 iff the
    ; packet's type code is > 1500 (DIX), but on my machine, it's
    ; always 1.
    mov     ax, word [di]
    xchg    ah, al
    cmp     ax, 1501    ; CF=1 if IEEE 802.3
    sbb     ax, ax
    add     ax, ax      ; -2 if IEEE 802.3, 0 if DIX

    sub     di, ax
    and     al, 0b1010  ; 10 if IEEE 802.3, 0 if DIX
    inc     ax

    mov     cl, 8
    call    find_matching_handle    ; Result in SI
    jnz     do_nothing

    xor     ax, ax
    mov     cx, bx
    mov     bx, si
    call    call_receiver

    mov     ax, es
    or      ax, di
    jz      do_nothing      ; Receiver returned NULL
    push    di
    push    cx
    mov     si, rfds + 16
    rep     movsb

    mov     ax, 1
    push    es
    pop     ds
    pop     cx
    pop     si
    ; INTENTIONAL FALLTHROUGH TO CALL_RECEIVER!

;浜様様様様様様様様様様様様様�
;�       CALL_RECEIVER       �
;藩様様様様様様様様様様様様様�
call_receiver:
    ; AX = flag (0=>buffer request, 1=>buffer acknowledge)
    ; BX = handle
    ; CX = length
    ; (if AX==1) DS:SI = buffer
    push    cx

    ; Flags are already nonzero because of the STI in _hw_int
    push    word 0          ; 0 = Sentintel
    pushf                   ; In case they use iret
    call    far [cs:bx + 11]
    sti                     ; In case they cleared it

    pop     cx
    jcxz    .done   ; They used iret
    pop     cx
.done:
    pop     cx
    ; INTENTIONAL FALLTHROUGH TO DO_NOTHING!

do_nothing:
    ret
