; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

    ;   struct handle {                 // size = 16 bytes
    ;0      valid               : 8     // either 0 or 1
    ;1      if_class            : 8
    ;2      packet_type_length  : 8
    ;3      packet_type         : 64
    ;11     receiver            : 32
    ;15     padding             : 8
    ;   }

handles equ 0x80

;浜様様様様様様様様様様様様様様融
;�       FIND_FREE_HANDLE       �
;藩様様様様様様様様様様様様様様夕
find_free_handle:
    ; Returns DI = handle or CF=1 if no free handles
    mov     di, handles + 16 * 7
.loop:
    cmp     byte [cs:di], 0
    je      .done
    sub     di, 16
    cmp     di, handles
    jnc     .loop
.done:
    ret

;浜様様様様様様様様様様様様様�
;�       VERIFY_HANDLE       �
;藩様様様様様様様様様様様様様�
verify_handle:
    ; AX = handle
    ; Returns CF = 0 if the handle is valid
    test    ax, 0xff0f  ; Must be on a paragraph boundary
    stc
    jnz     .done
    cmp     al, 0x80    ; And from 0x80 to 0xf0
    jc      .done

    xchg    ax, si
    cmp     byte [cs:si], 1     ; CF=1 if 'valid' byte not set
    xchg    ax, si
.done:
    ret

;浜様様様様様様様様様様様様様様様様融
;�       FIND_MATCHING_HANDLE       �
;藩様様様様様様様様様様様様様様様様夕
find_matching_handle:
    ; ES:DI = packet type
    ; CL = packet type length
    ; AL = if_class (must be 1 or 11)
    ; Returns SI = handle or ZF=0 if no match
    push    ds

    mov     si, handles + 16 * 7
    push    cs
    pop     ds
.loop:
    push    ax, cx, si
    mov     ah, al
    lodsb               ; Get 'valid' byte
    sub     ah, al      ; if_class - 1 if valid, else if_class
    lodsb               ; Get 'if_class' byte

    dec     ax          ; if_class - 1, or in an edge case
                        ; (uninitialized if_class), AX becomes
                        ; 0100 - 1 = 00ff or 0b00 - 1 = 0aff
    cmp     al, ah
    jnz     .next_handle

    lodsb               ; Get 'packet_type_length' byte
                        ; Compare MIN(AL,CL) bytes
    sub     al, cl
    sbb     ah, ah      ; LT: -1        GTE: 0
    and     al, ah      ; LT: AL - CL   GTE: 0
    add     cl, al      ; LT: CL+AL-CL  GTE: CL

    sub     ch, ch      ; Preset ZF and make CX valid
    push    di
    repe    cmpsb
    pop     di
.next_handle:
    pop     si, cx, ax
    jz      .loop_done
    sub     si, 16
    cmp     si, handles
    jnc     .loop
.loop_done:
    pop     ds
    ret

;浜様様様様様様様様様様様様様様様�
;�       VERIFY_ONE_HANDLE       �
;藩様様様様様様様様様様様様様様様�
nhandles equ verify_one_handle.a
verify_one_handle:
    ; Word [BP] = handle
    ; Mangles AX
    ; Returns CF=0 if the handle is valid and it's the only open handle
    ; If the handle was invalid, BL is ERROR_BAD_HANDLE, else BL = BH
    mov     ax, word [bp]
    call    verify_handle
    mov     bl, ERROR_BAD_HANDLE
    jc      .done
    mov     bl, bh
    db      0xb0        ; MOV AL,IMMED8
.a: db      0
    cmp     al, 2
    cmc
.done:
    ret
