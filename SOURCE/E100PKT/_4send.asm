; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

f_send_pkt:
    ; DS:SI = buffer, CX = length
    cmp     cx, 1515        ; CF=1 if it's small enough
    cmc
    jc      .done

    pushad
    push    es, cs
    pop     es
    xor     eax, eax
    mov     bx, .tcb
    mov     [es:bx], ax     ; TCB status = 0

    push    ax, si
    pop     esi             ; ESI &= 0xffff
    mov     ax, ds
    shl     eax, 4
    add     eax, esi

    mov     di, .tbd
    stosd               ; Buffer # 0 address
    xchg    ax, cx
    stosw               ; Buffer # 0 size

    xchg    ax, bx      ; AX = .tcb
    cwde                ; EAX = TCB address relative to CU_BASE
    mov     bx, 0x10    ; CUC = CU_START
    call    scb_command
    xchg    ax, di
.wait:                  ; Wait until the adapter writes to the status
    mov     al, byte [es:di + 1]
    and     al, 0b1011_0000 ; Isolate complete/ok/underrun
    jz      .wait
    xor     al, 0b1010_0000 ; 0 if no errors
    cmp     al, 1
    cmc

    pop     es
    popad
.done:
    mov     bl, ERROR_CANT_SEND
    ret
align 2
; WARNING: .tcb must be less than 0x8000 because of the CWDE above
.tcb:   dw 0        ; Status
        dw 0x800c   ; EL=1, SF=1 (flexible), CMD=100
        dd -1       ; Link address = don't care
        dd .tbd     ; TBD array address relative to CU_BASE
        dw 0        ; TCB Byte Count = 0
        dw 0x01e0   ; TBD # = 1, Transmit Threshold = max legal value
                    ; to avoid buffer underruns
.tbd:   dd -1       ; Buffer # 0 address (filled in by f_send_pkt)
        dw 0        ; Buffer # 0 size (filled in by f_send_pkt)
        dw 0        ; Reserved - must be 0
