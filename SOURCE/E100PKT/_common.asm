; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

ERROR_BAD_HANDLE        EQU 1
ERROR_NO_CLASS          EQU 2
ERROR_NO_TYPE           EQU 3
ERROR_NO_NUMBER         EQU 4
ERROR_BAD_TYPE          EQU 5
ERROR_NO_MULTICAST      EQU 6
ERROR_CANT_TERMINATE    EQU 7
ERROR_BAD_MODE          EQU 8
ERROR_NO_SPACE          EQU 9
ERROR_TYPE_INUSE        EQU 10
ERROR_BAD_COMMAND       EQU 11
ERROR_CANT_SEND         EQU 12
ERROR_CANT_SET          EQU 13
ERROR_BAD_ADDRESS       EQU 14
ERROR_CANT_RESET        EQU 15

%macro push 1-*
    %rep %0
    push %1
    %rotate 1
    %endrep
%endmacro
%macro pop 1-*
    %rep %0
    pop %1
    %rotate 1
    %endrep
%endmacro

;浜様様様様様様様様様様様様�
;�       SCB_COMMAND       �
;藩様様様様様様様様様様様様�
csr_io_bar_plus2    equ scb_command.a
scb_command:
    ; EAX = SCB General Pointer
    ; BX = command word
    ; Must be called with interrupts enabled
    ; Doesn't mangle anything
    push    dx, cx
    db      0xba        ; MOV DX,IMMED16
.a: dw      0           ; SCB Command Word
    xchg    ax, cx

    cli
.wait:
    ; Wait until the previous command (if any) finished
    in      al, dx
    cmp     al, 0
    jne     .wait

    xchg    ax, cx
    times 2 inc dx  ; SCB General Pointer
    out     dx, eax

    times 2 dec dx  ; SCB Command Word
    xchg    ax, bx
    out     dx, ax
    sti

    xchg    ax, bx
    pop     cx, dx
    ret

;浜様様様様様様様様様様様様様様様�
;�       F_ERR_BAD_COMMAND       �
;藩様様様様様様様様様様様様様様様�
f_err_bad_command:
    mov     bl, ERROR_BAD_COMMAND
    stc
    ret
