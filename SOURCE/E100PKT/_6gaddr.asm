; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

f_get_address:
    ; BX = handle
    ; ES:DI = buffer
    ; CX = buffer length (in), actual length (out)
    ;
    ; Returns CF=1 and DH = error code on error, or
    ; CF=0 and CX = length
    cmp     cx, 6
    mov     bl, ERROR_NO_SPACE
    jc      .done

    push    si, di
    mov     si, mac_address
    mov     cx, 6
    times 3 cs movsw
    pop     di, si
    clc
.done:
    ret
