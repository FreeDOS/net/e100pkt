; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様様様様様様様様様様融
;�       DETECT_PCI_BUS_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様様夕
detect_pci_bus_or_quit:
    mov     ax, 0xb101  ; PCI installation check
    int     0x1a

    cmp     edx, "PCI "
    mov     dx, .nopci_str
    jne     print_message_and_quit
    test    ah, ah
    jnz     print_message_and_quit

    mov     dx, .no_mechanism1_str
    test    al, 1
    jz      print_message_and_quit
    ret

.nopci_str: db `\r\nError! No PCI bus was found.$`
.no_mechanism1_str: db `\r\nError! Your PCI bus doesn't support `
                    db `configuration space access mechanism 1.$`


;浜様様様様様様様様様様様様様様様様�
;�       DETECT_E100_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様�
detect_e100_or_quit:
    mov     si, .device_ids
.loop:
    lodsw               ; Fetch device id
    test    ax, ax
    mov     dx, .not_found_str
    jz      print_message_and_quit
    push    ax

    push    si
    xor     si, si      ; Index = 0 (Find the first one)
    xchg    ax, cx      ; CX := Device ID
    mov     dx, 0x8086  ; Vendor ID is Intel for all of them
    mov     ax, 0xb102  ; Find PCI Device
    int     0x1a
    pop     si

    test    ah, ah
    pop     cx
    jnz     .loop

    ; Otherwise, 0 => success
    movzx   eax, bx
    shl     eax, 8
    or      eax, 0x80000000
    mov     dword [pci_bdf], eax

    mov     dx, .found_str1
    xchg    ax, bx
    call    print_str_word

    mov     dx, .found_str2
    xchg    ax, cx
    call    print_str_word
    jmp     crlf

.found_str1: db `Found E100 with BusDeviceFunction $`
.found_str2: db ` and Vendor:Device id 8086:$`
.not_found_str: db `\r\nError! E100 not found.$`

.device_ids: dw 0x1029, 0x1030, 0x1031, 0x1032
             dw 0x1033, 0x1034, 0x1038, 0x1039
             dw 0x103a, 0x103b, 0x103c, 0x103d
             dw 0x103e, 0x1050, 0x1051, 0x1052
             dw 0x1053, 0x1054, 0x1055, 0x1056
             dw 0x1057, 0x1059, 0x1064, 0x1065
             dw 0x1066, 0x1067, 0x1068, 0x1069
             dw 0x106a, 0x106b, 0x1091, 0x1092
             dw 0x1093, 0x1094, 0x1095, 0x1209
             dw 0x1229, 0x2449, 0x2459, 0x245d
             dw 0x27dc, 0

;浜様様様様様様様様様様様様様様様様様融
;�       GET_CSR_IO_BAR_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様様夕
; Returns AX = CSR size
get_csr_io_bar_or_quit:
    mov     bl, 0x14            ; CSR IO Mapped Base Address Register
    call    get_pci_var
    mov     dx, .csr_is
    call    print_str_dword
    xchg    eax, ebp            ; EBP = raw BAR

    xor     eax, eax
    dec     eax
    call    write_pci_var	; See which bits are hardwired to 0.
    call    get_pci_var
    and     al, 0xfc
    neg     eax
    mov     dx, .size_is
    call    print_str_dword
    call    crlf

    sub     eax, 16
    xchg    eax, ebp            ; EAX = raw BAR, EBP = size
    call    write_pci_var
    cmp     ebp, 500		; Require 16 <= size <= 512.
    mov     dx, .badsize	; WARNING: dump_* assumes size <= 512.
    ja      print_message_and_quit
    add     bp, 16

    test    al, 1
    mov     dx, .not_io
    jz      print_message_and_quit
    and     al, 0xfc            ; Get rid of information bits
    mov     word [csr_io_bar], ax
    inc     ax
    mov     word [csr_io_bar_plus1], ax
    inc     ax
    mov     word [csr_io_bar_plus2], ax

    push    ax
    add     eax, ebp
    mov     dx, .too_big
    jc      print_message_and_quit
    cmp     eax, 0x1_0002
    ja      print_message_and_quit
    pop     ax
    dec     ax
    dec     ax
    mov     dx, .null
    jz      print_message_and_quit
    xchg    ax, bp
    ret

.csr_is:    db "CSR IO Mapped BAR: Value = $"
.size_is:   db ", Size = $"
.badsize:   db 13,10,"Error: Bad size$"
.not_io:    db 13,10,"Error: Expected an IO BAR, but got a memory "
            db "mapped BAR$"
.null:      db 13,10,"Error: The CSR IO BAR is null$"
.too_big:   db 13,10,"Error: The CSR IO BAR doesn't fit in 16 bits$"
section .bss
    csr_io_bar: resw 1
section .text
;浜様様様様様様様様様様様様様様様�
;�       SELF_TEST_OR_QUIT       �
;藩様様様様様様様様様様様様様様様�
self_test_or_quit:
    mov     ax, word [csr_io_bar]
    add     al, 8	; s/AX/AL okay because 16-aligned.
    xchg    ax, dx	; PORT

    ; Bits 31:4 is paragraph-aligned result pointer
    ; Bits 3:0 is 0001
    xor     eax, eax
    mov     ax, ds
    shl     eax, 4
    add     eax, .selftest_result + 1
    out     dx, eax
    call    sleep_10ms

    mov     si, .selftest_result
    mov     dx, .timed_out_str
    lodsd
    test    eax, eax
    jz      print_message_and_quit
    lodsd
    test    eax, eax
    jnz     .failed

    mov     dx, .passed_str
    jmp     print_str
.failed:
    mov     dx, .failed_str
    call    print_str_dword
    jmp     quit_error

align 16
.selftest_result: times 4 db 0       ; CROM Signature
                  times 4 db 0xff    ; Test result

.timed_out_str: db `\r\nError! PORT self-test timed out (CROM `
                db `signature is still 0).$`
.failed_str: db `\r\nError! PORT self-test failed with result $`
.passed_str: db `PORT self-test passed.`
crlfstr: db 0xd,0xa,"$"

;浜様様様様様様様様様様様様�
;�       GET_PCI_VAR       �
;藩様様様様様様様様様様様様�
get_pci_var:
    ; BL = var index (divisible by 4)
    ; Returns EAX = var value
    call    write_pci_var.common
    in      eax, dx
    ret

;浜様様様様様様様様様様様様様�
;�       WRITE_PCI_VAR       �
;藩様様様様様様様様様様様様様�
pci_bdf equ write_pci_var.a
write_pci_var:
    ; BL = var index (divisible by 4)
    ; EAX = value to write
    push    eax
    call    .common
    pop     eax
    out     dx, eax
    ret
.common:
    db      0x66, 0xb8      ; MOV EAX,IMMED32
.a: dd      0
    mov     al, bl
    mov     dx, 0xcf8
    out     dx, eax
    mov     dl, 0xfc
    ret
