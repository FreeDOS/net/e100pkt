; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様融
;�       DUMP       �
;藩様様様様様様様様夕
dump:
    call    detect_pci_bus_or_quit
    call    detect_e100_or_quit
    call    dump_pci_to_file_or_quit

    call    get_csr_io_bar_or_quit	; Returns AX = CSR size
    call    dump_csr_to_file_or_quit	; Passed AX = CSR size

    call    timer_init
    call    dump_eeprom_to_file_or_quit
    call    timer_cleanup

    mov     dx, .didit_str
    jmp     print_message_and_quit_okay

.didit_str: db `\r\nThe dump was successful.$`

;浜様様様様様様様様様様様様様様様様様様融
;�       DUMP_PCI_TO_FILE_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様様様夕
dump_pci_to_file_or_quit:
    mov     di, eeprom_dump
    mov     bl, 0
.loop:
    call    get_pci_var
    stosd
    add     bl, 4
    jnc     .loop

    mov     dx, .fname
    mov     bp, 256
    jmp     dump_to_file_or_quit

.fname: db `E100.PCI`,0

;浜様様様様様様様様様様様様様様様様様様融
;�       DUMP_CSR_TO_FILE_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様様様夕
dump_csr_to_file_or_quit:
    mov     dx, word [csr_io_bar]
    mov     di, eeprom_dump
    xchg    ax, cx	; CX = CSR size
    push    cx
.loop:
    in      al, dx
    stosb
    inc     dx
    loop    .loop

    mov     dx, .fname
    pop     bp		; CSR size
    jmp     dump_to_file_or_quit

.fname: db `E100.CSR`,0

;浜様様様様様様様様様様様様様様様様様様様様�
;�       DUMP_EEPROM_TO_FILE_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様様様様様�
dump_eeprom_to_file_or_quit_fname: db "E100.EEP",0
dump_eeprom_to_file_or_quit:
    call    dump_eeprom_or_quit ; AX = # 16-bit registers
    add     ax, ax              ; Make that # bytes
    xchg    ax, bp              ; # bytes to write
    mov     dx, dump_eeprom_to_file_or_quit_fname
    ; INTENTIONAL FALLTHROUGH TO DUMP_TO_FILE_OR_QUIT!

;浜様様様様様様様様様様様様様様様様融
;�       DUMP_TO_FILE_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様夕
dump_to_file_or_quit:
    ; DS:DX = filename
    ; BP = # bytes to write
    ; Assumes that the data to write is in DS:eeprom_dump
    mov     ah, 0x3c    ; Create or truncate file
    xor     cx, cx      ; Normal attributes
    int     0x21        ; AX := handle to the filename DS:DX
    mov     dx, .disk_error_str
    jc      print_message_and_quit

    push    dx
    mov     dx, eeprom_dump ; Address of data
    xchg    ax, bx          ; BX = handle
    mov     ah, 0x40        ; Write to file or device
    mov     cx, bp          ; # bytes to write
    int     0x21
    pop     dx
    pushf

    mov     ah, 0x3e    ; Close file
    int     0x21
    popf
    jc      print_message_and_quit
    ret

.disk_error_str: db `\r\nError creating or writing to the file.$`
