; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

section .bss
    ; WARNING: dump_* and get_csr_io_bar assumes this is 512.
    eeprom_dump: resb 256 * 2       ; Max 256 16-bit registers
section .text

EESK EQU 1  ; Serial clock
EECS EQU 2  ; Chip select
EEDI EQU 4  ; Data in
EEDO EQU 8  ; Data out

;浜様様様様様様様様様様様融
;�       WRITE_EEDI       �
;藩様様様様様様様様様様様夕
set_eedi:
    stc
write_eedi:
    ; DX = EEPROM Control Register
    ; CF=1 to write a 1, 0 otherwise
    salc
    and     al, EEDI
    or      al, EECS
    out     dx, al
    call    sleep_3us

    or      al, EESK
    out     dx, al
    jmp     sleep_3us

;浜様様様様様様様様様様様様様様様様融
;�       READ_EEPROM_REGISTER       �
;藩様様様様様様様様様様様様様様様様夕
read_eeprom_register:
    ; CX = register number (MSB of r# is MSB of CX)
    ; Returns BX = register contents
    ;         BP = # address bits
    mov     ax, word [csr_io_bar]
    add     al, 0xe
    xchg    ax, dx      ; EEPROM Control Register

    ; Chip select
    mov     al, EECS | EESK
    out     dx, al
    call    sleep_3us

    ; Write Read Opcode (110)
    call    set_eedi
    call    set_eedi
    clc
    call    write_eedi

    ; Write into the address field until we get a 0 in EEDO:
    xor     bp, bp
.write_addr:
    inc     bp
    add     cx, cx
    call    write_eedi
    in      al, dx
    test    al, EEDO
    jnz     .write_addr

    ; Read the register: (CH already 0)
    mov     cl, 16      ; Loop once per bit, BX will hold the result
.loop2:
    clc
    call    write_eedi  ; Do a clock cycle
    in      al, dx
    and     al, EEDO
    cmp     al, 1
    adc     bx, bx      ; Write ~EEDO to LSB
    loop    .loop2
    not     bx

    ; Chip deselect
    mov     al, 0
    out     dx, al
    jmp     sleep_3us

;浜様様様様様様様様様様様様様様様様�
;�       DUMP_EEPROM_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様�
dump_eeprom_or_quit:
    ; Puts the data in eeprom_dump
    ; Returns AX = # 16-bit registers
    xor     cx, cx                  ; Read register 0
    call    read_eeprom_register    ; BP := # address bits
    xchg    ax, bp
    mov     dx, .field_size
    call    print_str_word
    cmp     ax, 6
    je      .ok
    cmp     ax, 8
    mov     dx, .bad_size
    jne     print_message_and_quit
.ok:
    ; Starting reg = 1 << (16 - nregister_bits)
    ; Reg increment = Starting reg
    ; # registers = 1 << nregister_bits
    mov     dx, 1
    push    dx
    push    ax
    neg     al
    add     al, 16
    xchg    ax, cx
    shl     dx, cl  ; DX := Reg increment

    xor     cx, cx  ; CX := Current reg
    mov     di, eeprom_dump
.loop:
    push    cx
    push    dx
    call    read_eeprom_register

    xchg    ax, bx  ; AX := value
    stosw

    pop     dx      ; Reg increment
    pop     cx      ; Current reg
    add     cx, dx
    jnc     .loop

    pop     cx      ; # address bits
    pop     ax      ; 1
    shl     ax, cl
    jmp     crlf

.field_size: db "Number of bits in the EEPROM address field is $"
.bad_size:   db 13,10,13,10,"Error: The spec says that the EEPROM "
             db "has 64 or 256 16-bit registers$"

;浜様様様様様様様様様様様様様様様様様様�
;�       GET_MAC_ADDRESS_OR_QUIT       �
;藩様様様様様様様様様様様様様様様様様様�
get_mac_address_or_quit:
    call    dump_eeprom_or_quit      ; Returns AX=# words

    call    check_eeprom_checksum
    mov     dx, .sum_is_str
    call    print_str_word

    cmp     ax, 0xbaba
    mov     dx, .bad_sum_str
    jne     print_message_and_quit

    mov     dx, .your_mac_is
    call    print_str
    mov     si, eeprom_dump
    mov     di, mac_address
    mov     cx, 6
    jmp     .lin

.lout:
    mov     ah, 2
    mov     dl, ':'
    int     0x21
.lin:
    lodsb
    stosb
    call    print_byte
    loop    .lout

    jmp     crlf

.sum_is_str: db `EEPROM checksum = $`
.bad_sum_str: db `.\r\n\r\nError! It should be BABA. Aborting.\r\n$`
.your_mac_is: db `\r\nMAC Address: $`

;浜様様様様様様様様様様様様様様様様様�
;�       CHECK_EEPROM_CHECKSUM       �
;藩様様様様様様様様様様様様様様様様様�
check_eeprom_checksum:
    ; AX = # words in the EEPROM
    ; Returns the checksum (should be 0xBABA) in AX
    ;
    ; The checksum is the sum of all the words in the EEPROM.
    xchg    ax, cx
    mov     si, eeprom_dump
    xor     bx, bx
.loop:
    lodsw
    add     bx, ax
    loop    .loop

    xchg    ax, bx
    ret

