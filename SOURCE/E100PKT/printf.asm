; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様融
;�       ATOI       �
;藩様様様様様様様様夕
atoi:
    ; AL = most significant digit
    ; AH = other digit
    ; Returns AL = number, or CF=1 on error
    call    atoi_internal
    jc      .done
    xchg    al, ah
    call    atoi_internal
    jc      .done

    ; AH is most significant now. Return AH * 16 + AL
    aad     16
    clc
.done:
    ret

;浜様様様様様様様様様様様様様�
;�       ATOI_INTERNAL       �
;藩様様様様様様様様様様様様様�
atoi_internal:
    ; AL = char, returns AL = num or CF=1 on error
    ;
    ; 0-9 is 0x30-0x39
    ; a-f is 0x61-0x66
    ; A-F is 0x41-0x46
    cmp     al, '0'
    jb      .err
    cmp     al, '9'
    jbe     .digit

    cmp     al, 'A'
    jb      .err
    cmp     al, 'F'
    jbe     .letter
    cmp     al, 'a'
    jb      .err
    cmp     al, 'f'
    jnbe    .err

.letter:
    or      al, 0x20    ; tolower
    sub     al, 'a' - 10
    ret
.digit:
    sub     al, '0'
    ret
.err:
    stc
    ret

;浜様様様様様様様様融
;�       CRLF       �
;藩様様様様様様様様夕
crlf:
    ; CS=DS
    push    ax
    push    dx

    mov     ah, 9
    mov     dx, crlfstr
    int     0x21

    pop     dx
    pop     ax
    ret

;浜様様様様様様様様様様様�
;�       PRINT_STR       �
;藩様様様様様様様様様様様�
print_str:
    ; DS:DX = str
    push    ax
    mov     ah, 9
    int     0x21
    pop     ax
    ret

;浜様様様様様様様様様様様様様融
;�       PRINT_STR_BYTE       �
;藩様様様様様様様様様様様様様夕
print_str_byte:
    ; DS:DX = str, AL = byte
    call    print_str
    jmp     print_byte

;浜様様様様様様様様様様様様様融
;�       PRINT_STR_WORD       �
;藩様様様様様様様様様様様様様夕
print_str_word:
    ; DS:DX = str, AX = word
    call    print_str
    jmp     print_word

;浜様様様様様様様様様様様様様様�
;�       PRINT_STR_DWORD       �
;藩様様様様様様様様様様様様様様�
print_str_dword:
    ; DS:DX = str, EAX = dword
    call    print_str
    ; INTENTIONAL FALLTHROUGH TO PRINT_DWORD!

;浜様様様様様様様様様様様様�
;�       PRINT_DWORD       �
;藩様様様様様様様様様様様様�
print_dword:
    ; EAX = dword to print
    rol     eax, 16
    call    print_word
    rol     eax, 16
    ; INTENTIONAL FALLTHROUGH TO PRINT_WORD!

;浜様様様様様様様様様様様融
;�       PRINT_WORD       �
;藩様様様様様様様様様様様夕
print_word:
    ; AX = word to print
    xchg    ah, al
    call    print_byte
    xchg    ah, al
    ; INTENTIONAL FALLTHROUGH TO PRINT_BYTE!

;浜様様様様様様様様様様様融
;�       PRINT_BYTE       �
;藩様様様様様様様様様様様夕
print_byte:
    ; AL = byte to print
    push    ax
    shr     al, 4
    call    print_nibble
    pop     ax
    ; INTENTIONAL FALLTHROUGH TO PRINT_NIBBLE!

;浜様様様様様様様様様様様様融
;�       PRINT_NIBBLE       �
;藩様様様様様様様様様様様様夕
print_nibble:
    ; AL (bits 0-3) = nibble to print
    push    ax, dx

    and     al, 0xf
    cmp     al, 10  ; 0-9: CF=1,      a-f: CF=0
    sbb     al, 0x69; 0-9: 0x90-0x99, a-f: 0xa1-0xa6
    das             ; 0-9: 0x30-0x39, a-f: 0x41-0x46
    or      al, 0x20; tolower

    xchg    ax, dx
    mov     ah, 2
    int     0x21
    pop     dx, ax
    ret
