; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様様融
;�       HW_INT       �
;藩様様様様様様様様様夕
pic_port_lo         equ hw_int.a
out_or_nop          equ hw_int.b
prev_hw_int         equ hw_int.c
csr_io_bar_plus1    equ hw_int.d
hw_int:
    pushad

    ; Acknowledge interrupts on the device
    db      0xba        ; MOV DX,IMMED16
.d: dw      0           ; SCB Interrupt Acknowledge Byte
    in      al, dx
    out     dx, al
    cbw
    xchg    ax, cx
    jcxz    .notus     ; CL = interrupt flags

    ; Acknowledge interrupts on the PIC
    mov     al, 0x20
    db      0xe6        ; OUT IMMED8,AL
.a: db      0x20        ; init code may change this to 0xa0
.b: dw      0x9090      ; init code may change this to 0x20e6, which
                        ; is OUT 0x20,AL
    sti
    cld
    add     cl, cl      ; Only one we care about is
    jns     .handled_fr ; FR (frame received), which is 1 << 6

    push    ds
    push    es
    times 2 push cs
    pop     ds
    pop     es

    call    handle_fr
    call    reset_rfds
    pop     es
    pop     ds
.handled_fr:
    popad
    iret

.notus:
    popad
    db      0xea    ; JMP IMMED32
.c: dd      0

