; E100PKT, packet driver for DOS
; Copyright (C) 2018, Seth Simon (sethsimon@sdf.org)
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see
; <https://www.gnu.org/licenses/>.

;浜様様様様様様様様様様様様様様融
;�       GET_ARGS_OR_QUIT       �
;藩様様様様様様様様様様様様様様夕
get_args_or_quit:
    ; Jumps to unload(), dump(), or load()
    
    ; 0x80 has strlen of args, 0x81 has args
    mov     dx, usage_str
    mov     si, 0x80
    lodsb
    cmp     al, 0
    je      print_message_and_quit

    call    eat_spaces
    or      ax, 0x2002  ; 10.1101 (-) becomes 10.1111 (/)
                        ; Lowercase or mangle the second character
                        ; ("?" is already 0x3f)
    cmp     al, '/'
    jne     print_message_and_quit

    cmp     ah, 'h'
    je      print_message_and_quit_okay
    cmp     ah, '?'
    je      print_message_and_quit_okay

    ; /V => Do nothing since the copyright message has the version
    cmp     ah, 'v'
    je      quit_okay

    cmp     ah, 'd'
    je      dump

    cmp     ah, 'u'
    jne     .parse_install_args_or_quit
    call    eat_spaces
    cmp     al, 0xd
    jne     print_message_and_quit
    jmp     unload

.parse_install_args_or_quit:
    cmp     ah, 'i'
    jne     print_message_and_quit

    ; Get int_no
    call    eat_spaces
    call    atoi
    jc      print_message_and_quit
    mov     byte [pkt_int], al

    ; Make sure int_no is 60-66, 68-6f, or 7b-7e
    mov     di, valid_sw_vecs
    mov     cx, valid_sw_vecs_end - valid_sw_vecs
    repne   scasb
    jne     print_message_and_quit

    call    eat_spaces
    cmp     al, 0xd
    jne     print_message_and_quit
    jmp     load

;浜様様様様様様様様様様様融
;�       EAT_SPACES       �
;藩様様様様様様様様様様様夕
eat_spaces:
    ; DS:SI = pointer to possible space (in/out)
    ; Returns AL = first char after space
    ;         AH = second char after space
    dec     si
.loop:
    inc     si
    cmp     byte [si], ' '
    je      .loop
    cmp     byte [si], 9
    je      .loop

    lodsw
    ret

; WARNING: GET_E100_INT assumes that the first entry in this list
; is 0x60
valid_sw_vecs: db 0x60, 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x68, 0x69
               db 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x7b, 0x7c, 0x7d
               db 0x7e
valid_sw_vecs_end:
; WARNING: GET_E100_INT assumes that the first byte following the
; end of the sw vector list is below 0x60 ('U' = 0x55)
usage_str:
    db "Usage: e100pkt <option>",13,10
    db "Options are case-insensitive and begin with / or -",13,10
    db "Options:",13,10
    db "    [/h | /?]       Show this usage message",13,10
    db "    /v              Show version",13,10
    db "    /u              Unload E100PKT",13,10
    db "    /i <int_no>     Install E100PKT at software interrupt <int_no>",13,10
    db "                    <int_no> is 60-66, 68-6f, or 7b-7e (inclusive)",13,10
    db "    /d              Dump PCI configuration space to E100.PCI",13,10
    db "                    Dump EEPROM to E100.EEP",13,10
    db "                    Dump CSR to E100.CSR$"
